package de.arlab.formulas;

import de.arlab.util.ToBeImplementedException;

import java.util.Map;

/**
 * Class for an AND-formula.
 */
public final class And extends BinaryFormula {

    /**
     * Constructor for an AND-formula.
     *
     * @param left  left formula in infix notation
     * @param right right formula in infix notation
     */
    public And(final Formula left, final Formula right) {
        super(left, right);
    }

    @Override
    public boolean evaluate(final Map<Variable, Boolean> assignment) throws IllegalArgumentException {
        return left.evaluate(assignment) && right.evaluate(assignment);
    }

    /**
     * tests whether the current formula is structurally syntactically equal to the formula f
     *
     * @param other formula to compare with the current formula
     * @return Boolean
     */
    @Override
    public boolean syntEqual(final Formula other) {
        if (other instanceof And)
            return this.getLeft().syntEqual(((And) other).getLeft()) && this.getRight().syntEqual(((And) other).getRight());

        return false;
    }

    /***
     *Simplification of this
     * @return Formula
     */
    @Override
    public Formula simplify() {
        if (this.getRight().syntEqual(this.getLeft())) return this.getRight().simplify();
        if (this.getRight() instanceof Verum) return this.getLeft().simplify();
        if (this.getLeft() instanceof Verum) return this.getRight().simplify();
        if ((this.getRight() instanceof Falsum) || (this.getLeft() instanceof Falsum))
            return FALSUM;
        return new And(this.getLeft().simplify(), this.getRight().simplify());
    }

    /**
     * which is a new formula
     * in which all occurrences of var were replaced by formula
     *
     * @param var     the variable to substitute
     * @param formula the formula to replace the variable
     * @return Formula
     */
    @Override
    public Formula substitute(final Variable var, final Formula formula) {

        return new And(asVariable(var, formula, this.getLeft()), asVariable(var, formula, this.getRight()));
    }

    /**
     * asVariable recursively find var and replace with formula
     *
     * @param var
     * @param formula
     * @param me_Formula : this.getLeft or getRight
     * @return Formular
     */
    public Formula asVariable(final Variable var, final Formula formula, final Formula me_Formula) {
        if ((me_Formula instanceof Variable) && (me_Formula.equals(var))) return formula;
        return me_Formula.substitute(var, formula);
    }


    /***
     * isAtomicFormula informs us if this is Atomic
     * @return Boolean (true if this is Atomic,false if not)
     */
    @Override
    public boolean isAtomicFormula() {
        return false;
    }

    /***
     * isLiteral informs us if this is Literal
     * @return Boolean (true if this is Literal,false if not)
     */
    @Override
    public boolean isLiteral() {
        return false;
    }

    /***
     * isClause informs us if this is Clause
     * @return Boolean (true if this is Clause,false if not)
     */
    @Override
    public boolean isClause() {
        return false;
    }

    /***
     * isMintern informs us if this is mintern
     * @return Boolean (true if this is Mintern,false if not)
     */
    @Override
    public boolean isMinterm() {
        return this.getLeft().isMinterm() && this.getRight().isMinterm();

    }

    /***
     * isNNf informs us if this is NNF
     * @return Boolean (true if this is NNF,false if not)
     */
    @Override
    public boolean isNNF() {
        return this.getLeft().isNNF() && this.getRight().isNNF();
    }

    /***
     * isCNf informs us if this is CNF
     * @return Boolean (true if this is CNF,false if not)
     */
    @Override
    public boolean isCNF() {
        return this.getLeft().isCNF() && this.getRight().isCNF();
    }

    /***
     * isDNf informs us if this is DNF
     * @return Boolean (true if this is DNF,false if not)
     */
    @Override
    public boolean isDNF() {
        return this.getLeft().isMinterm() && this.getRight().isMinterm();
    }

    /**
     * nnf calculate  Negation Normal Form
     *
     * @return formula in nnf
     */
    @Override
    public Formula nnf() {
        return new And(this.getLeft().nnf(), this.getRight().nnf()).simplify();
    }

    /**
     * cnf calculate conjunction  Normal Form
     *
     * @return formula in CNF
     */
    @Override
    public Formula cnf() {
        return new And(this.getLeft().cnf(), this.getRight().cnf()).simplify();
    }


    /**
     * dnf calculate Disjunctive normal form
     *
     * @return formula in DNF
     * CNF : Algo
     * (B or d) and C) (b and c) or (d and c)
     * A or (B and C) ==> (A and b) or (A and C)
     */
    @Override
    public Formula dnf() {

        if (this.getLeft() instanceof Or)
            return new Or(
                    new And(((Or) this.getLeft()).getLeft(), this.getRight()).simplify(),
                    new And(((Or) this.getLeft()).getRight(), this.getRight()).simplify()).dnf();
        if (this.getRight() instanceof Or)
            return new Or(
                    new And(this.getLeft(), ((Or) this.getRight()).getLeft()).simplify(),
                    new And(this.getLeft(), ((Or) this.getRight()).getRight()).simplify()).dnf();

        return this.simplify();

    }

    @Override
    public int hashCode() {
        return 23 * left.hashCode() + 29 * right.hashCode();
    }

    @Override
    public String toString() {
        return "(" + left + " AND " + right + ")";
    }
}