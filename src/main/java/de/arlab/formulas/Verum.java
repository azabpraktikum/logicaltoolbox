package de.arlab.formulas;

import de.arlab.util.ToBeImplementedException;

import java.util.Map;

/**
 * Class for the constant true.
 */
public final class Verum extends Formula {

    private static final Verum instance = new Verum();

    /**
     * Private constructor for singleton pattern.
     */
    private Verum() {
    }

    /**
     * Returns the single instance of the Verum class.
     *
     * @return Verum object
     */
    public static Verum mk() {
        return instance;
    }

    @Override
    public boolean evaluate(final Map<Variable, Boolean> assignment) throws IllegalArgumentException {
        return true;
    }

    /**
     * tests whether the current formula is structurally syntactically equal to the formula f
     *
     * @param other formula to compare with the current formula
     * @return Boolean
     */
    @Override
    public boolean syntEqual(final Formula other) {
        return (other instanceof Verum);
    }

    /***
     *Simplification of this
     * @return Formula
     */
    @Override
    public Formula simplify() {
        return this;
    }

    /**
     * which is a new formula
     * in which all occurrences of var were replaced by formula
     *
     * @param var     the variable to substitute
     * @param formula the formula to replace the variable
     * @return Formula
     */
    @Override
    public Formula substitute(final Variable var, final Formula formula) {
        return this;
    }

    /***
     * isAtomicFormula informs us if this is Atomic
     * @return Boolean (true if this is Atomic,false if not)
     */
    @Override
    public boolean isAtomicFormula() {
        return true;
    }

    /***
     * isLiteral informs us if this is Literal
     * @return Boolean (true if this is Literal,false if not)
     */
    @Override
    public boolean isLiteral() {
        return true;
    }

    /***
     * isClause informs us if this is Clause
     * @return Boolean (true if this is Clause,false if not)
     */
    @Override
    public boolean isClause() {
        return true;
    }

    /***
     * isMintern informs us if this is mintern
     * @return Boolean (true if this is Mintern,false if not)
     */
    @Override
    public boolean isMinterm() {
        return true;
    }

    /***
     * isNNf informs us if this is NNF
     * @return Boolean (true if this is NNF,false if not)
     */
    @Override
    public boolean isNNF() {
        return true;
    }

    /***
     * isCNf informs us if this is CNF
     * @return Boolean (true if this is CNF,false if not)
     */
    @Override
    public boolean isCNF() {
        return true;
    }

    /***
     * isDNf informs us if this is DNF
     * @return Boolean (true if this is DNF,false if not)
     */
    @Override
    public boolean isDNF() {
        return true;
    }

    /**
     * nnf calculate  Negation Normal Form
     *
     * @return formula in nnf
     */
    @Override
    public Formula nnf() {
        return this;
    }

    /**
     * cnf calculate conjunction  Normal Form
     *
     * @return formula in CNF
     */
    @Override
    public Formula cnf() {
        return this;
    }

    /**
     * dnf calculate Disjunctive normal form
     *
     * @return formula in DNF
     */
    @Override
    public Formula dnf() {
        return this;
    }

    @Override
    public int hashCode() {
        return 0x42;
    }

    @Override
    public String toString() {
        return "1";
    }
}
