package de.arlab.formulas;

import de.arlab.util.ToBeImplementedException;

import java.util.Map;

/**
 * Class for an OR-formula.
 */
public final class Or extends BinaryFormula {

    /**
     * Constructor for OR-formula.
     *
     * @param left  left formula in infix notation
     * @param right right formula in infix notation
     */
    public Or(final Formula left, final Formula right) {
        super(left, right);
    }

    @Override
    public boolean evaluate(final Map<Variable, Boolean> assignment) throws IllegalArgumentException {
        return left.evaluate(assignment) || right.evaluate(assignment);
    }

    /**
     * tests whether the current formula is structurally syntactically equal to the formula f
     *
     * @param other formula to compare with the current formula
     * @return Boolean
     */
    @Override
    public boolean syntEqual(final Formula other) {
        if (other instanceof Or)
            return this.getLeft().syntEqual(((Or) other).getLeft()) && this.getRight().syntEqual(((Or) other).getRight());

        return false;
    }

    /***
     *Simplification of this
     * @return Formula
     */
    @Override
    public Formula simplify() {
        if (this.getRight().syntEqual(this.getLeft())) return this.getRight().simplify();
        if ((this.getRight() instanceof Verum) || (this.getLeft() instanceof Verum))
            return VERUM;
        if (this.getRight() instanceof Falsum) return this.getLeft().simplify();
        if (this.getLeft() instanceof Falsum) return this.getRight().simplify();

        return new Or(this.getLeft().simplify(), this.getRight().simplify());
    }

    /**
     * which is a new formula
     * in which all occurrences of var were replaced by formula
     *
     * @param var     the variable to substitute
     * @param formula the formula to replace the variable
     * @return Formula
     */
    @Override
    public Formula substitute(final Variable var, final Formula formula) {
        return new Or(asVariable(var, formula, this.getLeft()), asVariable(var, formula, this.getRight()));
    }

    /**
     * asVariable recursively find var and replace with formula
     *
     * @param var
     * @param formula
     * @param me_Formula : this.getLeft or getRight
     * @return Formular
     */
    public Formula asVariable(final Variable var, final Formula formula, final Formula me_Formula) {
        if ((me_Formula instanceof Variable) && (me_Formula.equals(var))) return formula;
        return me_Formula.substitute(var, formula);
    }

    /***
     * isAtomicFormula informs us if this is Atomic
     * @return Boolean (true if this is Atomic,false if not)
     */
    @Override
    public boolean isAtomicFormula() {
        return false;
    }

    /***
     * isLiteral informs us if this is Literal
     * @return Boolean (true if this is Literal,false if not)
     */
    @Override
    public boolean isLiteral() {
        return false;
    }

    /***
     * isClause informs us if this is Clause
     * @return Boolean (true if this is Clause,false if not)
     */
    @Override
    public boolean isClause() {
        return this.getLeft().isClause() && this.getRight().isClause();
    }

    /***
     * isMintern informs us if this is mintern
     * @return Boolean (true if this is Mintern,false if not)
     */
    @Override
    public boolean isMinterm() {
        return false;
    }

    /***
     * isNNf informs us if this is NNF
     * @return Boolean (true if this is NNF,false if not)
     */
    @Override
    public boolean isNNF() {
        return this.getLeft().isNNF() && this.getRight().isNNF();
    }

    @Override
/***
 * isCNf informs us if this is CNF
 * @return Boolean (true if this is CNF,false if not)
 */
    public boolean isCNF() {
        return this.getLeft().isClause() && this.getRight().isClause();
    }

    /***
     * isDNf informs us if this is DNF
     * @return Boolean (true if this is DNF,false if not)
     */
    @Override
    public boolean isDNF() {
        return this.getLeft().isDNF() && this.getRight().isDNF();
    }

    /**
     * nnf calculate  Negation Normal Form
     *
     * @return formula in nnf
     */
    @Override
    public Formula nnf() {
        if (this.getLeft() instanceof And)
            return new Or(
                    new And(((And) this.getLeft()).getLeft().simplify(), this.getRight().simplify()),
                    new And(((And) this.getLeft()).getRight().simplify(), this.getRight().simplify())).nnf();

        if (this.getRight() instanceof And)
            return new Or(
                    new And(this.getLeft().simplify(), ((And) this.getRight()).getLeft().simplify()),
                    new And(this.getLeft().simplify(), ((And) this.getRight()).getRight().simplify())).nnf();
        return new Or(this.getLeft().nnf(), this.getRight().nnf()).simplify();
    }

    @Override
    /**
     * cnf calculate conjunction  Normal Form
     * @return formula in CNF
     * CNF : Algo
     *
     * A and (B or C) = (A or b) and (A or C)
     */
    public Formula cnf() {
        if (this.getLeft() instanceof And)
            return new And(
                    new Or(((And) this.getLeft()).getLeft(), this.getRight()).cnf(),
                    new Or(((And) this.getLeft()).getRight(), this.getRight()).cnf()
            ).cnf();
        if (this.getRight() instanceof And)
            return new And(
                    new Or(this.getLeft(), ((And) this.getRight()).getLeft()).cnf(),
                    new Or(this.getLeft(), ((And) this.getRight()).getRight()).cnf()
            ).cnf();
        return this.simplify();
    }

    /**
     * dnf calculate Disjunctive normal form
     *
     * @return formula in DNF
     */
    @Override
    public Formula dnf() {

        return new Or(this.getLeft().dnf(), this.getRight().dnf()).simplify();
    }

    @Override
    public int hashCode() {
        return 17 * left.hashCode() + 19 * right.hashCode();
    }

    @Override
    public String toString() {
        return "(" + left + " OR " + right + ")";
    }
}
