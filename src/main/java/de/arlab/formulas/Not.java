package de.arlab.formulas;

import de.arlab.util.ToBeImplementedException;

import java.util.Map;

/**
 * Class for a NOT-formula.
 */
public final class Not extends Formula {

  private final Formula operand;

  /**
   * Constructor for NOT-formula.
   * @param operand operand of the not formula
   */
  public Not(final Formula operand) {
    this.operand = operand;
  }

  /**
   * Returns the operand of this Not formula.
   * @return operand of this Not formula
   */
  public Formula getOperand() {
    return operand;
  }

  @Override
  public boolean evaluate(final Map<Variable, Boolean> assignment) throws IllegalArgumentException {
    return !operand.evaluate(assignment);
  }

    /**
     * tests whether the current formula is structurally syntactically equal to the formula f
     * @param other formula to compare with the current formula
     * @return Boolean
     */
  @Override
  public boolean syntEqual(final Formula other) {
    if (other instanceof  Not){
      return this.getOperand().syntEqual(((Not) other).getOperand());
    }
    else return false ;

  }
    /***
     *Simplification of this
     * @return Formula
     */
  @Override
  public Formula simplify() {
    if (this.getOperand() instanceof Not )
        return ((Not) this.getOperand()).getOperand().simplify();
    if (this.getOperand()  instanceof Falsum)
     return VERUM;

    if (this.getOperand()  instanceof Verum)
          return FALSUM;
      return new Not(this.getOperand().simplify());
  }

    /**
     * which is a new formula
     in which all occurrences of var were replaced by formula
     *
     * @param var     the variable to substitute
     * @param formula the formula to replace the variable
     * @return Formula
     */
  @Override
  public Formula substitute(final Variable var, final Formula formula) {
      return new Not(asVariable(var,formula,this.getOperand()));
  }

    /**
     *
     * asVariable recursively find var and replace with formula
     * @param var Variable
     * @param formula Formula
     * @param me_Formula : this.getLeft or getRight
     * @return Formular
     */
    public Formula asVariable(final Variable var,final Formula formula,final Formula me_Formula){
        if((me_Formula instanceof Variable) && (me_Formula.equals(var))) return formula ;
        return  me_Formula.substitute(var,formula);
    }

    /***
     * isAtomicFormula informs us if this is Atomic
     * @return Boolean (true if this is Atomic,false if not)
     */

    @Override
  public boolean isAtomicFormula() { return false; }
    /***
     * isLiteral informs us if this is Literal
     * @return Boolean (true if this is Literal,false if not)
     */
  @Override
  public boolean isLiteral() {
  if ((this.getOperand() instanceof Variable)  || (this.getOperand() instanceof Falsum)  || (this.getOperand() instanceof Verum) )
    return true;
  return false;
  }
    /***
     * isClause informs us if this is Clause
     * @return Boolean (true if this is Clause,false if not)
     */
  @Override
  public boolean isClause() {
      if ((this.getOperand() instanceof Variable)  || (this.getOperand() instanceof Falsum)  || (this.getOperand() instanceof Verum) )
          return true;
      return false;
  }
    /***
     * isMintern informs us if this is mintern
     * @return Boolean (true if this is Mintern,false if not)
     */
  @Override
  public boolean isMinterm() {
      if ((this.getOperand() instanceof Variable)  || (this.getOperand() instanceof Falsum)  || (this.getOperand() instanceof Verum) )
          return true;
      return false;
  }

    /***
     * isNNf informs us if this is NNF
     * @return Boolean (true if this is NNF,false if not)
     */
  @Override
  public boolean isNNF(){
      if ((this.getOperand() instanceof Variable)  || (this.getOperand() instanceof Falsum)  || (this.getOperand() instanceof Verum) )
          return true;
      return false;
  }
    /***
     * isCNf informs us if this is CNF
     * @return Boolean (true if this is CNF,false if not)
     */
  @Override
  public boolean isCNF() {
      if ((this.getOperand() instanceof Variable)  || (this.getOperand() instanceof Falsum)  || (this.getOperand() instanceof Verum) )
          return true;
      return false;
  }
    /***
     * isDNf informs us if this is DNF
     * @return Boolean (true if this is DNF,false if not)
     */
  @Override
  public boolean isDNF() {
      if ((this.getOperand() instanceof Variable)  || (this.getOperand() instanceof Falsum)  || (this.getOperand() instanceof Verum) )
          return true;
      return false;
  }

    /**
     * nnf calculate Negation Normal Form
     * @return formula in nnf
     */
  @Override
  public Formula nnf() {
      if(this.getOperand() instanceof Not)
          return ((Not) this.getOperand()).getOperand().nnf();
    if((this.getOperand()instanceof Or)|| (this.getOperand()instanceof And))
        return this.applyDeMorgan(this).nnf();

    return this.simplify();
  }
    /**
     * cnf calculate conjunction  Normal Form
     * @return formula in CNF
     */
  @Override
  public Formula cnf() {
      if(this.getOperand() instanceof Not)
          return ((Not) this.getOperand()).getOperand().cnf();
      if((this.getOperand()instanceof Or)|| (this.getOperand()instanceof And))
          return this.applyDeMorgan(this).cnf().simplify();
      return this.simplify();
  }

    /**
     * dnf calculate Disjunctive normal form
     *
     * @return formula in DNF
     */
  @Override
  public Formula dnf() {
      if(this.getOperand() instanceof Not)
          return ((Not) this.getOperand()).getOperand().dnf();
      if((this.getOperand()instanceof Or)|| (this.getOperand()instanceof And))
          return this.applyDeMorgan(this).nnf().simplify();
      return this.simplify();
  }

  @Override
  public int hashCode() {
    return 13 * operand.hashCode();
  }

  @Override
  public String toString() {
    return "(" + "NOT " + operand + ")";
  }
}