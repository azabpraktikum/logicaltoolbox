package de.arlab.sat;

import de.arlab.formulas.And;
import de.arlab.formulas.Falsum;
import de.arlab.formulas.Formula;
import de.arlab.formulas.Not;
import de.arlab.formulas.Or;
import de.arlab.formulas.Variable;
import de.arlab.formulas.Verum;
import de.arlab.util.ToBeImplementedException;

import java.util.*;

/**
 * A mutable Clause.
 * A clause is a set of literals.
 * In propositional logic it is interpreted as a disjunction of the contained literals.
 */
public class Clause {

    private final Set<Literal> literals;

    /**
     * Constructs an empty clause.
     */
    public Clause() {
        literals = new HashSet<>();
    }

    /**
     * Constructor for a unit clause.
     *
     * @param lit the single literal in this clause
     */
    public Clause(final Literal lit) {
        literals = new HashSet<>();
        literals.add(lit);
    }

    /**
     * Constructor for a clause with a given collection of literals.
     *
     * @param lits the literals of this clause
     */
    public Clause(final Collection<Literal> lits) {
        literals = new HashSet<>(lits);
    }

    /**
     * Constructor for a clause with given literals.
     *
     * @param lits the literals of this clause
     */
    public Clause(final Literal... lits) {
        this.literals = new HashSet<>();
        Collections.addAll(literals, lits);
    }

    /**
     * Returns the literals of this clause.
     *
     * @return the literals of this clause
     */
    public Set<Literal> getLiterals() {
        return literals;
    }


    /**
     * Adds a literal to this clause.
     *
     * @param lit the literal to add
     */
    public void addLiteral(final Literal lit) {
        this.literals.add(lit);
    }

    /**
     * Removes a literal from this clause.
     *
     * @param lit the literal to remove
     */
    public void removeLiteral(final Literal lit) {
        this.literals.remove(lit);
    }

    /**
     * Returns {@code true} if this clause is unit.
     *
     * @return {@code true} if this clause is unit, otherwise {@code false}
     */
    public boolean isUnit() {
        if (this.literals.size() == 1) return true;
        return false;
    }

    /**
     * Returns {@code true} if this clause is empty.
     *
     * @return {@code true} if this clause is empty, otherwise {@code false}
     */
    public boolean isEmpty() {
        return this.literals.isEmpty();
    }

    /**
     * Returns the single literal of this clause, if this clause is a unit clause. Otherwise null will be returned.
     *
     * @return the single literal of this clause, or null if this clause is not unit
     */
    public Literal getUnitLit() {
        if (isUnit())
            return literals.iterator().next();
        else
            return null;
    }

    /**
     * Returns {@code true} if this clause contains a given literal literal.
     *
     * @param lit the literal
     * @return {@code true} if this clause contains lit, otherwise {@code false}
     */
    public boolean contains(final Literal lit) {
        for (Literal elem : this.literals) {
            if (elem.equals(lit)) return true;
        }
        return false;
    }

    /**
     * Adds all literals of the given clause to this clause.
     *
     * @param c clause to union with
     */
    public void unionWith(final Clause c) {

        this.literals.addAll(c.getLiterals());
    }

    @Override
    public boolean equals(final Object other) {
        if (other == this)
            return true;
        if (other == null)
            return false;
        return other instanceof Clause && literals.equals(((Clause) other).literals);
    }

    @Override
    public int hashCode() {
        int sum = 0;
        for (Literal lit : literals)
            sum += lit.hashCode();
        return sum;
    }

    @Override
    public String toString() {
        return literals.toString();
    }


    /**
     * Converts an arbitrary formula to a set of clauses.
     *
     * @param f the formula
     * @return a set of clauses equivalent to the CNF of the formula
     */
    public static Set<Clause> formula2Clauses(final Formula f) {
        Formula formula = f.cnf();
        // System.out.println("nnf"+formula.toString());
        HashSet<Clause> listClauses = new HashSet<Clause>();
        //Clause cl = new Clause();
        if (formula instanceof And) {
            if (((And) formula).getLeft() instanceof And) {
                listClauses.addAll(formula2Clauses(((And) formula).getLeft()));
            } else {
                listClauses.add(new Clause(mkClaus(((And) formula).getLeft())));
            }

            if (((And) formula).getRight() instanceof And) {
                listClauses.addAll(formula2Clauses(((And) formula).getRight()));
            } else {
                listClauses.add(new Clause(mkClaus(((And) formula).getRight())));
            }


        } else listClauses.add(new Clause(mkClaus(formula)));
        return listClauses;
    }

    /**
     * Make Set of Literal from OFormula
     *
     * @param f : formula but not AndFormula
     * @return Set of Literal
     */
    public static Set<Literal> mkClaus(final Formula f) {
        HashSet<Literal> listLiteral = new HashSet<Literal>();
        if (f instanceof Not) {
            if (((Not) f).getOperand() instanceof Variable) {
                Variable var1 = new Variable(((Variable) ((Not) f).getOperand()).getName());
                listLiteral.add(new Literal(var1, false));
            }
        }
        if (f instanceof Variable) {
            Variable var = new Variable(((Variable) f).getName());
            listLiteral.add(new Literal(var, true));
        }
        if (f instanceof Or) {
            listLiteral.addAll(mkClaus(((Or) f).getLeft()));
            listLiteral.addAll(mkClaus(((Or) f).getRight()));
        }

        return listLiteral;
    }


    /**
     * Converts a set of clauses into a corresponding formula in cnf.
     *
     * @param clauses the set of clauses
     * @return the correspondin formula in cnf
     */
    public static Formula clauses2Formula(final Set<Clause> clauses) {
        Formula formula = Verum.mk();
        for (Clause elem : clauses) {
            formula = new And(formula, clauses2FormulaHelp(elem));
        }

        return formula.simplify();

    }

    /**
     * clauses2FormulaHelp :convert a clause int a Or formula
     *
     * @param cl Clause
     * @return
     */
    public static Formula clauses2FormulaHelp(final Clause cl) {
        Formula formula = Falsum.mk();
        for (Literal elem : cl.getLiterals())
            formula = new Or(formula, elem.getVar());

        return formula.simplify();

    }

    /**
     * Returns all variables which are present in a given set of clauses.
     *
     * @param clauses the set of clauses
     * @return all variables in the clauses
     */
    public static Set<Variable> vars(final Set<Clause> clauses) {
        final Set<Variable> vars = new HashSet<Variable>();
        for (Clause c : clauses)
            vars.addAll(vars(c));
        return vars;
    }

    private static Set<Variable> vars(final Clause c) {
        final Set<Variable> vars = new HashSet<Variable>();
        for (Literal l : c.getLiterals())
            vars.add(l.getVar());
        return vars;
    }
}
