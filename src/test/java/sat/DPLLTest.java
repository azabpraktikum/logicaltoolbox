package sat;

import de.arlab.formulas.*;
import de.arlab.sat.Clause;
import de.arlab.sat.DPLLSolver;
import de.arlab.sat.Literal;
import de.arlab.sat.heuristics.TrivialHeuristic;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DPLLTest {

    private static Formula x1 = F.var1;
    private static Formula x2 = F.var2;
    private static Formula x3 = F.var3;
    private static Formula x4 = F.var4;
    private static Formula x5 = F.var5;

    @Test
    public void testSolver() {
        assertTrue(true);
        //assertTrue(false);
    }

    @Test
    public void testdpll() {
        TrivialHeuristic tr = new TrivialHeuristic();
        DPLLSolver dpllSolver = new DPLLSolver(tr);

        dpllSolver.isContradiction(new And(Falsum.mk(), Verum.mk()));

        assertTrue(dpllSolver.isContradiction(new And(Falsum.mk(), Verum.mk())));
        assertFalse(dpllSolver.isContradiction(new And(x1, new Not(x1))));
        //assertTrue(false);

        Literal lit1 = new Literal(new Variable("var1"), true);
        Literal lit2 = new Literal(new Variable("var2"), true);
        Literal lit3 = new Literal(new Variable("var3"), true);
        Clause clause = new Clause();
        Clause cl = new Clause();
        Clause cl1 = new Clause();
        Clause cl2 = new Clause();

        clause.addLiteral(lit2);
        clause.addLiteral(lit3);

        cl.addLiteral(lit2);
        cl.addLiteral(lit1);

        //cl.unionWith(clause);
        List<Clause> clauseList = new ArrayList<>();
        List<Clause> clauseList1 = new ArrayList<>();
        clauseList.add(cl);
        clauseList1.add(cl1);
        clauseList1.add(clause);


        List<Clause> clauseSet = new ArrayList<>();
        cl2.addLiteral(lit2);
        cl1.addLiteral(lit2.negate());
        clauseSet.add(cl2);
        clauseSet.add(cl1);

        assertTrue(dpllSolver.dpll(clauseList));
        assertTrue(dpllSolver.dpll(clauseList1));
        assertFalse(dpllSolver.dpll(clauseSet));

    }
}
