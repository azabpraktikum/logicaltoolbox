package sat;

import de.arlab.formulas.*;
import de.arlab.sat.Clause;
import de.arlab.sat.DPLLSolver;
import de.arlab.sat.Literal;
import de.arlab.sat.heuristics.TrivialHeuristic;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Clausetest {
    private static Formula x1 = F.var1;
    private static Formula x2 = F.var2;
    private static Formula x3 = F.var3;
    private static Formula x4 = F.var4;
    private static Formula x5 = F.var5;

    @Test
    public void clauses2formulatest() {
        Literal lit1 = new Literal(new Variable("var1"), true);
        Literal lit2 = new Literal(new Variable("var2"), true);
        Literal lit3 = new Literal(new Variable("var3"), true);
        Clause clause = new Clause();
        Clause cl = new Clause();
        Clause cl1 = new Clause();
        Clause cl2 = new Clause();

        clause.addLiteral(lit2);
        clause.addLiteral(lit3);

        cl.addLiteral(lit2);
        cl.addLiteral(lit1);

        cl1.addLiteral(lit2);
        cl2.addLiteral(lit2);
        cl2.addLiteral(lit3);
        cl2.addLiteral(lit1);


        //cl.unionWith(clause);
        HashSet<Clause> clauseList = new HashSet<Clause>();
        HashSet<Clause> clauseList1 = new HashSet<Clause>();

        clauseList.add(cl);

        clauseList.add(clause);

        clauseList1.add(cl);
        clauseList1.add(clause);

        clauseList1.add(cl1);
        clauseList1.add(cl2);
        assertTrue(Clause.clauses2Formula(clauseList).toString().equals("((var2 OR var1) AND (var3 OR var2))"));
        assertTrue(Clause.clauses2Formula(clauseList1).toString().equals("((((var2 OR var1) AND var2) AND (var3 OR var2)) AND ((var3 OR var2) OR var1))"));


    }

    @Test
    public void formula2Clauses() {
        Formula nonCnf1 = new And(
                new Or(x1, x2),
                new Not(new Or(new Or(x1, x2), x3)));

        Formula test9 = new Or(new Not(x1), new Not(x2));
        assertTrue(Clause.formula2Clauses(test9).toString().equals("[[-x1, -x2]]"));
        assertTrue(Clause.formula2Clauses(nonCnf1).toString().equals("[[-x1], [-x2], [-x3], [x1, x2]]"));



    }

}
