package de.arlab.formulas;

import de.arlab.sat.Clause;
import de.arlab.sat.Literal;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FormulaTest {

  private static Map<Variable, Boolean> assignment = new HashMap<Variable, Boolean>();

  private static Formula nnf;
  private static Formula cnf;
  private static Formula nonCnf1;
  private static Formula nonCnf2;
  private static Formula dnf;
  private static Formula nonDnf1;
  private static Formula nonDnf2;

  private static Formula x1 = F.var1;
  private static Formula x2 = F.var2;
  private static Formula x3 = F.var3;
  private static Formula x4 = F.var4;
  private static Formula x5 = F.var5;

  @BeforeClass
  public static void initialize() {
    assignment.put(F.var1, true);
    assignment.put(F.var2, false);
    assignment.put(F.var3, true);
    assignment.put(F.var4, false);

    nnf = new And(new Not(F.var3), new Or(F.var1, F.var2));
    cnf = new And(new Or(new Not(x1), x2), new Or(new Or(x1, x2), x3));
    nonCnf1 = new And(
            new Or(x1, x2),
            new Not(new Or(new Or(x1, x2), x3)));
    nonCnf2 = new And(
            new Or(new Not(x1), x2),
            new And(new Or(x1, new And(x2, x1)), x3));

    dnf = new Or(new And(new Not(x1), x2), new And(new And(x1, x2), x3));
    nonDnf1 = new Or(
            new And(new Not(new Not(x1)), x2),
            new And(new And(x1, x2), x3));
    nonDnf2 = new Or(
            new And(new Not(x1), x2),
            new Or(new And(x1, new Or(x2, x1)), x3));
  }

  @Test
  public void testGettersAndSetters() {
    assertEquals(F.var1, new And(F.var1, F.var2).getLeft());
    assertEquals(F.var2, new And(F.var1, F.var2).getRight());
    assertEquals(F.var1, new Or(F.var1, F.var2).getLeft());
    assertEquals(F.var2, new Or(F.var1, F.var2).getRight());
  }

  @Test
  public void testCompare() {
    assertTrue(F.var1.compareTo(F.var2) != 0);
    assertTrue(F.var1.compareTo(F.var1) == 0);
  }

  @Test
  public void testEvaluate() {
    assertTrue(F.verum.evaluate(assignment));
    assertFalse(F.falsum.evaluate(assignment));
    assertTrue(F.var1.evaluate(assignment));
    assertFalse(F.var2.evaluate(assignment));
    assertTrue(F.var3.evaluate(assignment));
    assertFalse(F.var4.evaluate(assignment));
    assertFalse(F.f1.evaluate(assignment));
    assertFalse(F.f2.evaluate(assignment));
    assertTrue(F.f3.evaluate(assignment));
    assertTrue(F.f4.evaluate(assignment));
    assertTrue(F.f5.evaluate(assignment));
    assertFalse(F.f6.evaluate(assignment));
    assertTrue(F.f7.evaluate(assignment));
    assertFalse(F.f8.evaluate(assignment));
    assertTrue(F.f9.evaluate(assignment));
    assertTrue(F.f10.evaluate(assignment));
    assertTrue(F.f11.evaluate(assignment));
    assertFalse(F.f12.evaluate(assignment));
    assertTrue(F.f13.evaluate(assignment));




  }
  @Test
  public void testSyntEqual(){
    assertTrue(nonCnf2.syntEqual(nonCnf2));
    assertFalse(nonCnf2.syntEqual(nonCnf1));
    assertTrue(x1.syntEqual(x1));
    assertFalse(x1.syntEqual(x2));
  }
  @Test
  public void testSimplify(){
    Or test1 = new Or(x1,x1);
    And test2 = new And(test1,test1) ;
    Not test3 = new Not(x2);
    Not test4 = new Not(test3);
    Formula verum1 =Verum.mk();
    Formula falsum1= Falsum.mk();
    Not test5 = new Not(verum1);
    assertTrue(x1.equals(test1.simplify()));
    assertTrue(x1.equals(test2.simplify()));
    assertTrue(x2.equals(test4.simplify()));
    assertTrue(falsum1.equals(test5.simplify()));
  }
  @Test
  public void testSubstitution(){
    Or test1 = new Or(x1,x1);
    // nonDnf2 = (((NOT x1) AND x2) OR ((x1 AND (x2 OR x1)) OR x3))
    assertTrue(nonDnf2.substitute(F.var1,test1).toString().equals("(((NOT (x1 OR x1)) AND x2) OR (((x1 OR x1) AND (x2 OR (x1 OR x1))) OR x3))"));

  }
  @Test
  public void testPredicate(){
    Formula test6  = new And(new Not(F.var3), new And(new And(F.var1, Falsum.mk()), Verum.mk()));
    Formula test7  = new And(new Not(F.var3), new Or(F.var1, Verum.mk()));
    Formula test8  = new Or(new Not(F.var3), new Or(new Or(F.var1, Falsum.mk()), new Not(Verum.mk())));
    Not test5 = new Not(Verum.mk());

    assertTrue(F.var1.isAtomicFormula() && x1.isAtomicFormula() && Verum.mk().isAtomicFormula() && Falsum.mk().isAtomicFormula());
    assertFalse(test5.isAtomicFormula()  || nonCnf1.isAtomicFormula());
    assertTrue(F.var1.isLiteral() && x1.isLiteral() && Verum.mk().isLiteral() && Falsum.mk().isLiteral() && test5.isLiteral());
    assertFalse(cnf.isLiteral());

    assertTrue(test6.isMinterm());
    assertFalse(test7.isMinterm());
    assertTrue(test8.isClause());
    assertFalse(test6.isClause());
  }
  @Test
  public void testNormalPredicate(){
    Formula test9 = new And(new Not(F.var3), new Not(new And(F.var1, Falsum.mk())));
    Formula test10  = new And(new Not(F.var3), new Or(F.var1, Verum.mk()));
    Formula test11 = new Or(new Not(F.var3), new Or(new Or(F.var1, Falsum.mk()), new Not(Verum.mk())));

    assertFalse(test9.isNNF());
    assertTrue(test10.isNNF());

    assertFalse(new And(new Not(F.var3), new Or(x1,new And(F.var1, Falsum.mk()))).isCNF());
    assertTrue(test11.isCNF());

    assertTrue(test11.isDNF());
    assertTrue(new Or(new Not(F.var3), new Or(new And(F.var1, Falsum.mk()),new Or(F.var1, new And(F.var1, Falsum.mk())))).isDNF());
  }
  @Test
  public void testApplyDeMorgan(){
    Formula test9 = new Or(new Not(x1),new Not(x2));
    Formula test10  = new And(new Not(F.var3), new Or(F.var1, Verum.mk()));
    Formula test11 = new Or(new Not(F.var3), new Or(new Or(F.var1, Falsum.mk()), new Not(Verum.mk())));

    assertTrue(test9.equals(test9.applyDeMorgan(new Not(new And(x1,x2)))));
  }
  @Test
  public void testNnfCnfDnf(){
    assertTrue(new Not(new And(x1,Falsum.mk())).nnf().equals(Verum.mk()));
    assertTrue(nonCnf1.nnf().toString().equals("((x1 OR x2) AND (((NOT x1) AND (NOT x2)) AND (NOT x3)))"));
    //System.out.println(new Not(new And(x1,Falsum.mk())).nnf().toString());

    //dnf test
    assertTrue(nonDnf1.dnf().toString().equals("((x1 AND x2) OR ((x1 AND x2) AND x3))"));
    assertTrue(nonDnf2.dnf().toString().equals("(((NOT x1) AND x2) OR (((x1 AND x2) OR x1) OR x3))"));


    /**
     * CNF-test :
     *
     */
    //noncnf1 = ((x1 OR x2) AND (NOT ((x1 OR x2) OR x3)))
    assertTrue(nonCnf1.cnf().toString().equals("((x1 OR x2) AND (((NOT x1) AND (NOT x2)) AND (NOT x3)))"));
    //System.out.println(nonCnf1.cnf().toString());
    //System.out.println(nonCnf1.toString());

  }
  @Test
  public void dpllLiteralClaus(){
    Literal lit1= new Literal(new Variable("var1"),true);
    Literal lit2= new Literal(new Variable("var2"),true);
    Literal lit3= new Literal(new Variable("var3"),true);
    Clause clause=new Clause();
    Clause cl=new Clause();


    assertTrue(clause.isEmpty());
    clause.addLiteral(lit1);

    assertTrue(clause.isUnit());
    assertTrue(clause.contains(lit1));
    clause.addLiteral(lit2);
    clause.addLiteral(lit3);

    cl.addLiteral(lit2);

    cl.unionWith(clause);
    List<Clause> clauseList = new ArrayList<>();
    clauseList.add(cl);
    clauseList.add(clause);







    /**
     * CNF-test :
     *noncnf1 = ((x1 OR x2) AND (NOT ((x1 OR x2) OR x3)))
     */
    assertTrue(nonCnf1.cnf().toString().equals("((x1 OR x2) AND (((NOT x1) AND (NOT x2)) AND (NOT x3)))"));


  }
  @Test(expected = IllegalArgumentException.class)
  public void testEvaluateException() {
    F.f14.evaluate(assignment);
  }
}
